let shapes = []
let shapesTransformed = []

let canonicGrid
let alphaCanonicGrid
let rotation
let shearX
let shearY
let scaleX
let scaleY
let translateX
let translateY

let transformedGrid
let alphaTransformedGrid

let matrixConf
let settingsCanvas

let colorRed = [255, 0, 0, 255]
let colorWhite = [255, 255, 255, 255]

let M

function setup() {
  let canvas = createCanvas(800, 800);

  canvas.parent("canvas")
  canvas.mousePressed(addPoint)

  let button = createButton('play');
  button.mousePressed(calculate)
  matrixConf = document.getElementById('matrix');
  matrixConf.value = "0.7,-0.7,0\n0.7,0.7,0\n1,1,0"

  canonicGrid = new Grid(50, 50, colorWhite)
  shapes.push(canonicGrid)

  createWidget()
  buildMatrix()
  calculate()
  canonicGrid.setAlpha(100)
  transformedGrid.setAlpha(100)

}

function createWidget() {
  let settingsContainer = createDiv('')
  // Canonic base
  let settingsContainerCanonic = createDiv('')
  let labelAlphaCanonic = createElement('label', 'alpha canonic grid')
  alphaCanonicGrid = createSlider(0, 255, 100)
  alphaCanonicGrid.parent(settingsContainerCanonic)
  labelAlphaCanonic.parent(settingsContainerCanonic)
  alphaCanonicGrid.input(setAlphaCanonicGrid)
  // Transformed base
  let settingsContainerTransformed = createDiv('')
  let labelAlphaTransformed = createElement('label', 'alpha transformed grid')
  alphaTransformedGrid = createSlider(0, 255, 100)
  alphaTransformedGrid.parent(settingsContainerTransformed)
  labelAlphaTransformed.parent(settingsContainerTransformed)
  alphaTransformedGrid.input(setAlphaTransformedGrid)
  // Rotation
  let settingsContainerRotation = createDiv('')
  let labelRotation = createElement('label', 'rotation')
  rotation = createSlider(0, 2*PI, 0, 0)
  rotation.parent(settingsContainerRotation)
  labelRotation.parent(settingsContainerRotation)
  rotation.input(buildMatrix)
  // ShearX
  let settingsContainerShearX = createDiv('')
  let labelShearX = createElement('label', 'shearX')
  shearX = createSlider(-2, 2, 0, 0)
  shearX.parent(settingsContainerShearX)
  labelShearX.parent(settingsContainerShearX)
  shearX.input(buildMatrix)
  // ShearY
  let settingsContainerShearY = createDiv('')
  let labelShearY = createElement('label', 'shearY')
  shearY = createSlider(-2, 2, 0, 0)
  shearY.parent(settingsContainerShearY)
  labelShearY.parent(settingsContainerShearY)
  shearY.input(buildMatrix)
  // ShearX
  let settingsContainerScaleX = createDiv('')
  let labelScaleX = createElement('label', 'scaleX')
  scaleX = createSlider(-2, 2, 1, 0)
  scaleX.parent(settingsContainerScaleX)
  labelScaleX.parent(settingsContainerScaleX)
  scaleX.input(buildMatrix)
  // ShearY
  let settingsContainerScaleY = createDiv('')
  let labelScaleY = createElement('label', 'scaleY')
  scaleY = createSlider(-2, 2, 1, 0)
  scaleY.parent(settingsContainerScaleY)
  labelScaleY.parent(settingsContainerScaleY)
  scaleY.input(buildMatrix)
  // TranslateX
  let settingsContainerTranslateX = createDiv('')
  let labelTranslateX = createElement('label', 'translateX')
  translateX = createSlider(-width/2, width/2, 0, 0)
  translateX.parent(settingsContainerTranslateX)
  labelTranslateX.parent(settingsContainerTranslateX)
  translateX.input(buildMatrix)
  // TranslateY
  let settingsContainerTranslateY = createDiv('')
  let labelTranslateY = createElement('label', 'translateY')
  translateY = createSlider(-height/2, height/2, 0, 0)
  translateY.parent(settingsContainerTranslateY)
  labelTranslateY.parent(settingsContainerTranslateY)
  translateY.input(buildMatrix)
  // Put containers into main one
  settingsContainerCanonic.parent(settingsContainer)
  settingsContainerTransformed.parent(settingsContainer)
  settingsContainerRotation.parent(settingsContainer)
}

function setAlphaCanonicGrid() {
  let alpha = alphaCanonicGrid.value()
  canonicGrid.setAlpha(alpha)
}

function setAlphaTransformedGrid() {
  let alpha = alphaTransformedGrid.value()
  if (transformedGrid) {
    transformedGrid.setAlpha(alpha)
  }
}

function buildMatrix() {
  let angle = rotation.value()
  let a = shearX.value()
  let b = shearY.value()
  let tx = translateX.value()
  let ty = translateY.value()
  let sx = scaleX.value()
  let sy = scaleY.value()

  let alpha = sx*((1+a*b)*cos(angle)-b*sin(angle))
  let beta  = sx*((1+a*b)*sin(angle)+b*cos(angle))
  let gamma = sy*(a*cos(angle)-sin(angle))
  let delta = sy*(a*sin(angle)+cos(angle))

  let Delta = tx*(alpha-1) + ty*gamma
  let Gamma = tx*beta + ty*(delta-1)


  M = [
    [alpha, gamma, Delta],
    [beta, delta, Gamma],
    [0, 0, 1]
  ]
  calculate(M)

  let matrixString = M.map(row => row.join(',')).join('\n')

  matrixConf.value = matrixString
}

function getMatrixFromString() {
  M=[]
  let matrix = matrixConf.value
  let rows = matrix.split("\n")

  for(let i=0; i < rows.length; i++) {
    M.push([])
    let columns = rows[i].split(',')
    for(let j=0; j<columns.length; j++ ) {
      M[i].push(parseFloat(columns[j]))
    }
  }

  return M
}

function calculate(M=[]) {

  if (!M.length) {
    M = getMatrixFromString()
  }


  shapesTransformed = []

  for (let i = 0; i < shapes.length; i++) {

    let shape = shapes[i].transform(M)

    if(shape instanceof Grid) {

      transformedGrid = shape
    }

    shapesTransformed.push(shape)
  }
  setAlphaTransformedGrid()

}

function draw() {

  background(0)

  for (let i = 0; i < shapes.length; i++) {
    shapes[i].draw()
  }

  for (let i = 0; i < shapesTransformed.length; i++) {
    shapesTransformed[i].draw()
  }
}

function addPoint() {
  let x = mouseX-width/2
  let y = height/2-mouseY
  shapes.push(new Point(x, y, colorWhite))
  calculate(M)
}
