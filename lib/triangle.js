class Triangle extends Shape {

	constructor(a, b, c, col) {
		super(col)
		this.a = a
		this.b = b
		this.c = c
	}

	draw() {
		fill(this.color)
		stroke(this.color)
		triangle(this.a.x+width/2, height/2-this.a.y, this.b.x+width/2, height/2-this.b.y, this.c.x+width/2, height/2-this.c.y)
		noFill()
		noStroke()
	}

	transform(M) {
		let a = this.a.transform(M)
		let b = this.b.transform(M)
		let c = this.c.transform(M)

		return new Triangle(a, b, c, colorRed)
	}
}
