class Grid extends Shape {

	constructor(dx, dy, col) {
		super(col)
		this.dx = dx
		this.dy = dy
		this.delta = 1000
		this.shapes = []

		let a = 10
		let b = 5

		for (let x = -width/2-this.delta; x <  width/2+this.delta; x+=dx) {

			let line = new Line(new Point(x, -height/2-this.delta), new Point(x, height/2+this.delta), col)

			if (x === 0) {
				line.weight = 3
			}

			this.shapes.push(line)
		}

		for (let y = -height/2-this.delta; y <  height/2+this.delta; y+=dy) {

			let line = new Line(new Point(-width/2-this.delta, y), new Point(width/2+this.delta, y), col)

			if (y === 0) {
				line.weight = 3
			}

			this.shapes.push(line)
		}

		this.shapes.push(new Triangle(new Point(width/2-a, -b), new Point(width/2-a, b), new Point(width/2, 0), col))
		this.shapes.push(new Triangle(new Point(-b, height/2-a), new Point(b, height/2-a), new Point(0, height/2), col))

	}

	draw() {
		for(let i=0; i < this.shapes.length; i++) {
			this.shapes[i].draw()
		}
	}

	setAlpha(alpha) {

		let col = this.color.levels
		col[3] = alpha
		this.color = color(col[0], col[1], col[2], col[3])
		for(let i=0; i < this.shapes.length; i++) {
			this.shapes[i].setAlpha(alpha)
		}
	}

	transform(M) {
		let shapes = []
		for(let i=0; i < this.shapes.length; i++) {
			shapes.push(this.shapes[i].transform(M))
		}
		let grid = new Grid(this.dx, this.dy, colorRed)
		grid.shapes = shapes
		return grid
	}
}
