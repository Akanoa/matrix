class Point extends Shape {

	constructor(x, y, col) {
		super(col)
		this.x = x
		this.y = y
	}

	draw() {
		stroke(this.color)
		fill(this.color)
		ellipse(this.x+width/2, height/2-this.y, 5, 5);
		noFill()
		noStroke()
	}


	/**
	 * transform - Apply transformation M on this point
	 *
	 * @param  {type} M     description
	 * @return {Point}      A new point with transformed coordinates
	 */
	transform(M) {
		let point = super.transform(this.x, this.y, M)
		return new Point(point[0], point[1], colorRed)
	}
}
