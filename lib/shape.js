class Shape {

	/**
	 * constructor - description
	 *
	 * @param  {Color} col description
	 * @return {type}     description
	 */
	constructor(col=[0,0,0,0]) {
		colorMode(RGB)
		this.color =  color(col[0], col[1], col[2], col[3])
	}

	setAlpha(alpha) {

		let col = this.color.levels
		col[3] = alpha
		this.color = color(col[0], col[1], col[2], col[3])
	}

	draw() {
		throw "Not implemeted"
	}



	/**
	 * transform - Apply transformation M on point (x,y)
	 *
	 * @param  {float} x description
	 * @param  {float} y description
	 * @param  {array} M description
	 * @return {array}   A tuple of two values [x', y']
	 */
	transform(x, y, M) {

		let xPrime = M[0][0]*x + M[0][2] + M[0][1]*y
		let yPrime = M[1][0]*x + M[1][1]*y + M[1][2]

		return [xPrime, yPrime]
	}
}
