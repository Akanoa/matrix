class Line extends Shape {


  /**
   * constructor - Create a new line
   *
   * @param  {Point} start Starting point of the "line"
   * @param  {Point} end   Ending point of the "line"
   */
  constructor(start, end, col) {

    super(col)

    this.start = start
    this.end = end
    this.weight = 1
  }

  /**
   * draw - Draw a line
   *
   * @return {type}  description
   */
  draw() {
    fill(this.color)
    stroke(this.color)
    strokeWeight(this.weight)
    line(this.start.x+width/2, height/2-this.start.y, this.end.x+width/2, height/2-this.end.y)
    noFill()
    noStroke()
  }


  /**
   * transform - description
   *
   * @param  {type} M description
   * @return {type}   description
   */
  transform(M) {
    let start = this.start.transform(M)
    let end = this.end.transform(M)
    let line = new Line(start, end, colorRed)

    line.weight = this.weight
    return line
  }


}
